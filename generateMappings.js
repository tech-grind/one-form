const fs = require('fs')
const pdfFillForm = require('pdf-fill-form');

const files = fs.readdirSync('./resources');

files.forEach((fileName) => {
    pdfFillForm.read(`./resources/${fileName}`)
    .then(function(result) {
      fs.writeFileSync(`./formMappings/${fileName}.txt`, JSON.stringify(result, null, 2))
    }, function(err) {
	     console.log(err);
     });
   })
