const pdf = require('pdf-fill-form')
const fs = require('fs')
const archiver = require('archiver')

const saveSettings = {
    save: 'pdf',
    cores: 4,
    scale: 0.2,
    antialias: true
};

const writeFile = (mapFn, filename, data) => {
    return pdf.write(`resources/${filename}.pdf`,
              mapFn(data),
              saveSettings)
       .then ((result) => {
           fs.writeFile(`${filename}.pdf`, result , (err) => {
               if(err) {
                   return console.log(err)
               }
           })
       }, (err) => {
           console.log(err);
       })
}

const mapG1145 = (data) => ({
    'form1[0].#subform[0]﻿.FirstName[0]': data['first-name'],
    'form1[0].#subform[0]﻿.MiddleName[0]': data['middle-name'],
    'form1[0].#subform[0]﻿.LastName[0]': data['last-name'],
    'form1[0].#subform[0]﻿.Email[0]': data['email'],
    'form1[0].#subform[0]﻿.MobilePhoneNumber[0]': data['mobile-phone']
})

const g1145 = (data) => {
    return writeFile(mapG1145, 'g-1145', data)
}

mapN400 = (data) => ({
    'form1[0].#subform[0]﻿.P2_Line1_GivenName[0]': data['first-name'],
    'form1[0].#subform[0]﻿.P2_Line1_MiddleName[0]': data['middle-name'],
    'form1[0].#subform[0]﻿.P2_Line1_FamilyName[0]': data['last-name'],
    'form1[0].#subform[2]﻿.P4_Line5_Email[0]': data['email'],
    'form1[0].#subform[2]﻿.P4_Line4_Telephone[0]': data['mobile-phone'],
    'form1[0].#subform[5]﻿.P8_Line1_Occupation[0]': data['occupation']
})

const n400 = (data) => {
    return writeFile(mapN400, 'n-400', data)
}

const zipPdfs = (res) => () => {
    const output = fs.createWriteStream('forms.zip')
    const archive = archiver('zip')

    archive.pipe(res);
    archive.glob('*.pdf')
    return archive.finalize()
}

const unlinkCallback = (err) => {
    if (err) {
        console.log('Error deleting a file')
        console.log(err)
    }
}

const cleanupPdfs = () => {
    fs.unlink('n-400.pdf', unlinkCallback),
    fs.unlink('g-1145.pdf', unlinkCallback)
}

exports.createZip = (data, res) => {
    return Promise.all([n400(data), g1145(data)])
           .then(zipPdfs(res), (err) => console.log(err))
           .then(cleanupPdfs, (err) => console.log(err))
}
