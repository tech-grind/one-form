const { Translate } = require('@google-cloud/translate');

const translateText = (text, target, key) => {
  const translate = new Translate();

  return translate
    .translate(text, target)
    .then(results => {
      let translations = results[0];
      translations = Array.isArray(translations)
        ? translations
        : [translations];

      return translations;
    })
    .catch(err => {
      console.error('ERROR:', err);
    });
  };

  module.exports.translateText = translateText;
