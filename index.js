const express = require('express')
const bodyParser = require('body-parser')
const pdf = require('./pdfWriter')
const translateText = require('./utils/functions')

const port = 3000

const translationApiKey = process.env.GOOGLE_APPLICATION_CREDENTIALS;

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('static'))
app.set('view engine', 'pug')

app.get('/', (req, res) => {
    res.render('index', {
        title: 'One Form', heading: 'Welcome!'
    })
})

app.get('/naturalization', (req, res) => {
    res.render('naturalization', {
        title: 'One Form | Naturalization',
        heading: 'Naturalization'
    })
})

const mapInputs = (body) => {
  let hello = body;
  return translateText.translateText(body.occupation, 'en')
    .then(result => body.occupation = result[0]);
}

app.post('/naturalization', (req, res) => {
    const body = req.body

    console.log(body)

    mapInputs(body)
        .then((translation) => {
          console.log('final translation: ', body)
          res.attachment('forms.zip')
          pdf.createZip(body, res)
        })
})

app.listen(port, () => console.log(`One form listening on port ${port}!`))
