# One Form
One form to rule them all.

## Requirements

Install on OS X:
``` sh
$ brew install qt5 cairo poppler --with-qt5
```

Install on Ubuntu:

``` sh
$ sudo apt-get install libpoppler-qt5-dev libcairo2-dev
```

